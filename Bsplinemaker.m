%% B spline generator by Dr Joel A Rosenfeld for YouTube channel Th@MathThing
%% If you use this code, please give me credit, and link to the Splines YouTube video on my channel

% Knots
T = (0:0.1:1)';

% Order
m = 3;

n = length(T)-m-1;

% Evaluation Points for Plotting

evalt = 0:0.001:1;

% B-splines
N = @(x) double(x < T(2:end)).*double(x >= T(1:end-1));

figure
plotN = N(evalt);
plot(evalt,plotN(2:2:8,:),'LineWidth',3)


selectN = @(V,L) V(L);

for k = 1:m
    N = @(x) (x-T(1:(n+m-k)))./(T(k+1:(n+m))-T(1:(n+m-k))).*selectN(N(x),(1:(n+m-k))')...
        + (T((2+k):(n+m+1))-x)./(T((2+k):(n+m+1))-T(2:(n+m-k+1))).*selectN(N(x),(2:(n+m-k+1))');
end

TotalSplines = n+m-k;

plotN = zeros(length(N(0)),length(evalt));

for i = 1:length(evalt)
    plotN(:,i) = N(evalt(i));
end

figure

plot(evalt,plotN,'LineWidth',3)

figure

plot(evalt,sum(plotN,1),'LineWidth',3)

%%%%%%%%%%%

% Evaluation Points for Plotting

evalt = T(m):0.001:T(m+n-1);

plotN = zeros(length(N(0)),length(evalt));

for i = 1:length(evalt)
    plotN(:,i) = N(evalt(i));
end

% Get Points

figure

plot(0,0)
xlim([-1 1])
ylim([-1 1])
[xi,yi] = getpts;

xi = xi(1:TotalSplines);
yi = yi(1:TotalSplines);

hold on

plot(xi,yi,'bo')

plotApproximation = zeros(2,length(evalt));

for i = 1:TotalSplines
   plotApproximation = plotApproximation + [xi(i),yi(i)]'*plotN(i,:);
end

plot(plotApproximation(1,:),plotApproximation(2,:));