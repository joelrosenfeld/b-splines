%% Spline interpolation code by Dr. Joel A. Rosenfeld for YouTube channel Th@MathThing
%% If you use this code for a project, please give me credit, and link to the appropriate video

% Run writeme.m first

controlPoints = controlPoints(:,1:5:end);

% Order

m = 3;

% Knots
T=0:1/(size(controlPoints,2)+m):1;

%T=T(1:(end-1));


n = length(T)-m-1;

% Evaluation Points for Plotting

evalt = T(m+2):0.001:T(n);

% B-splines
N = @(x) double(x < T(2:end)).*double(x >= T(1:end-1));

selectN = @(V,L) V(L);

for k = 1:3
    N = @(x) (x-T(1:(n+m-k)))./(T(k+1:(n+m))-T(1:(n+m-k))).*selectN(N(x),(1:(n+m-k))')...
        + (T((2+k):(n+m+1))-x)./(T((2+k):(n+m+1))-T(2:(n+m-k+1))).*selectN(N(x),(2:(n+m-k+1))');
end

TotalSplines = n+m-k;

plotN = zeros(length(N(0)),length(evalt));

for i = 1:length(evalt)
    plotN(:,i) = N(evalt(i));
end

% Interpolation/Gram Matrix

evalMe = m:n+m-1;

GramMatrix = zeros(TotalSplines);

for i = 1:TotalSplines
    GramMatrix(i,:) = N(T(evalMe(i)));
end

Weights = pinv(GramMatrix)*controlPoints';

% Plot Approximation

plotApproximation = zeros(2,length(evalt));

for i = 1:TotalSplines
   plotApproximation = plotApproximation + [Weights(i,1),Weights(i,2)]'*plotN(i,:);
end


% plot(plotApproximation(1,:),plotApproximation(2,:));

figure

a1 = animatedline('Color',[0 .7 .7],'LineWidth',3);

axis([0 700 -800 0])
for k = 1:length(plotApproximation(2,:));
    % first line
    addpoints(a1,plotApproximation(1,k),plotApproximation(2,k));

    % update screen
    drawnow limitrate
    pause(0.001);
end

