%% Code by Joel Rosenfeld for YouTube Channel Th@MathThing's February 20th, 2021 Video on Splines
%% If you use this code for a project, please give me credit by linking to the corresponding video

%% This portion of the code takes a video of writing on an iPad and determined control points through differences between frames.

%% Video loading. (You might have to make your own)
M = VideoReader('atsymbol.mov');
allFrames = read(M);
whos allFrames

% Keeps frames in a separate space. In case we want to avoid loading everything again
saveallFrames = allFrames;

% Crop out top chunk of video.
allFrames = saveallFrames(1100:end,:,1,:); % Preprocessing

% Look for light and dark pixels and swap them with black and white respectively
allFrames = double(allFrames < 100);

% Take a difference between pixels
allFrames = allFrames(:,:,1:end-1) - allFrames(:,:,2:end);

% Black and White it again
allFrames = double(allFrames < 0);

% This gets rid of pixelation. Averaging left and right pixels as well as top and bottom pixels
allFrames =1/2*( allFrames(2:end,:,:) + allFrames(1:end-1,:,:));
allFrames = 1/2*( allFrames(:,2:end,:) + allFrames(:,1:end-1,:));

% Black and white it again.
allFrames = double(allFrames > 0.7);

% Delete blank frames
counter = 1;
for i = 1:length(allFrames(1,1,:))
    
    if(max(max(abs(allFrames(:,:,counter)))) == 0)
        allFrames(:,:,counter)= [];
        counter = counter-1;
    end
    counter = counter + 1;
end

% Animated figure. Comment this out to make it run faster.
figure
for i = 1:length(allFrames(1,1,:))
    pause(0.001)
    imshow(allFrames(:,:,i),[]);
end

% Get the control points by finding indexes of the first and last white pixels, then averaging them.
controlPoints = zeros(2,length(allFrames(1,1,:)));

for i = 1:length(allFrames(1,1,:))
   controlPoints(1,i) = 1/2*(find(max(allFrames(:,:,i)'),1,'first') + find(max(allFrames(:,:,i)'),1,'last'));
   controlPoints(2,i) = 1/2*(find(max(allFrames(:,:,i)),1,'first') + find(max(allFrames(:,:,i)),1,'last'));
end
RotationMatrix = [0,1;-1,0];
controlPoints = RotationMatrix*controlPoints;

figure
plot(controlPoints(1,:),controlPoints(2,:),'bo','LineWidth',3)
